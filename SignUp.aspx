﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="layout.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Arial_Rounded_MT_Bold_700.font.js" type="text/javascript"></script>
<script src="js/Myriad_Pro_600.font.js" type="text/javascript"></script>
<script src="js/Myriad_Pro_italic_600.font.js" type="text/javascript"></script>
<script src="js/PetitaLight_500.font.js" type="text/javascript"></script>
<script type="text/javascript">    
    function CheckBoxRequired_ClientValidate(sender, e) {
        e.IsValid = jQuery(".AcceptedAgreement input:checkbox").is(':checked');
    }
</script>
<!--[if lt IE 7]>
 <link href="ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body id="page9">
    <form id="form1" runat="server">
<div class="bg_top">
	<div id="main">
		<!--header -->
		<div id="header_noImage" class="wrapper">
			<div class="left">
					<a href="index.html"><img src="image/Learnexx3DLogo.png" alt="Learnexx 3D Virtual Equipment and Laboraties" /></a>
			</div>
			<div class="right">
					<div>
						<h5><span>Latest: </span><a href="News.html">Learnexx 3D wins Innovate UK Competition</a></h5>
					</div>
			</div>
			<div class="right">					
				<ul class="menu">
					<li><a href="index.html"><span>Home</span></a></li>
					<li><a href="LabSuppliers.html"><span>Lab Suppliers</span></a></li>
					<li><a href="Universities.html"><span>Universities</span></a></li>	
					<li><a href="News.html"><span>News</span></a></li>				
					<li><a href="Contact.html" class="last"><span>contact</span></a></li>
				</ul>
			</div>
		</div>
		<!--header end-->
		<div id="content" class="content">
			<div class="wrapper">
				<div class="col1">
					<div class="row1">
						<div class="tabs_container">						
							<div class="tab_content">
								    <h2>Virtual Science Lab Free Offer Registration</h2>
                                    <p>University, college lecturers and school teachers complete the form to use all existing 3D lab techniques and 1 lab method of your choice for free with your student cohort on the virtual science lab.
                                    We won't spam you or share your information with anyone else and only use it to validate that the details you provide are correct.</p>
                                    <div class="wrapper">
                                        <div class="signup1">
                                             <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
                                             <div class="signupTitleRow"><asp:Label ID="Label2" runat="server" Text="Name"></asp:Label></div>
                                             <div class="signupTitleRow"><asp:Label ID="Label3" runat="server" Text="Position"></asp:Label></div>
                                             <div class="signupTitleRow"><asp:Label ID="Label4" runat="server" Text="Department"></asp:Label></div>
                                             <div class="signupTitleRow"><asp:Label ID="Label5" runat="server" Text="Organisation"></asp:Label></div>
                                             <div class="signupTitleRow"><asp:Label ID="Label6" runat="server" Text="Email"></asp:Label></div>
                                             <div class="signupTitleRow"><asp:Label ID="Label7" runat="server" Text="Tel"></asp:Label></div>
                                        </div>
                                        <div class="signup2">
                                            <p><asp:TextBox ID="FirstName" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                    ControlToValidate="FirstName" ErrorMessage="RequiredFieldValidator">Please enter your first name</asp:RequiredFieldValidator>
                                            </p>
                                            <p><asp:TextBox ID="Name" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                    ControlToValidate="FirstName" ErrorMessage="RequiredFieldValidator">Please enter your first name</asp:RequiredFieldValidator></p>
                                            <p><asp:TextBox ID="Position" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                    ControlToValidate="Name" ErrorMessage="RequiredFieldValidator">Please enter your surname</asp:RequiredFieldValidator></p>
                                            <p><asp:TextBox ID="Department" runat="server" Width="350px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                    ControlToValidate="Department" ErrorMessage="RequiredFieldValidator">Please enter your school or department</asp:RequiredFieldValidator></p>
                                            <p><asp:TextBox ID="Organisation" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                    ControlToValidate="Organisation" ErrorMessage="RequiredFieldValidator">Please enter the organisation you work for</asp:RequiredFieldValidator></p>
                                            <p><asp:TextBox ID="Email" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                                    ControlToValidate="Email" ErrorMessage="RequiredFieldValidator">Please enter your email address</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                                    ControlToValidate="Email" ErrorMessage="Please enter a valid email address" 
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </p>
                                            <p><asp:TextBox ID="Tel" runat="server" Width="350px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                    ControlToValidate="Tel" ErrorMessage="RequiredFieldValidator">Please enter your telephone number</asp:RequiredFieldValidator>
                                            </p>
                                        </div>

                                        <div id="signupButtons" class="signup3" runat="server">
                                            <asp:CheckBox ID="TandCsCheckbox" runat="server" CssClass="AcceptedAgreement"/>&nbsp;&nbsp;I accept the <a href="Legal.html" target="_blank">virtual science lab offer terms and software licence agreement</a>
                                            <asp:CustomValidator runat="server" id="vTerms" EnableClientScript="true"
                                                ClientValidationFunction="CheckBoxRequired_ClientValidate">Please accept the terms and conditions to continue</asp:CustomValidator>
                                            <div class="signupTitleRow"><asp:ImageButton ID="ImageButton1" OnClick="btnSubmit_Click" CausesValidation="true" runat="server" ImageUrl="~/images/banner_link_bg.jpg" /><asp:LinkButton ID="LinkButton1" OnClick="btnSubmit_Click" CausesValidation="true" runat="server">Sign Up</asp:LinkButton></div>
                                        </div>
			                        </div>
                                <asp:Label ID="ResponseMessage" runat="server" CssClass="errormessage" Visible="False"></asp:Label>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!--footer -->
	<div id="footer">
		<div class="footer">
			<b>Solvexx Solutions Ltd</b>  &copy; 2016  <a href="Privacy.html" >Privacy Policy</a>,  <a href="Legal.html">Legal</a>
		</div>
	</div>
	<!--footer end-->
</div>
   <script type="text/javascript">       Cufon.now(); </script>
</form>
</body>

</html>


