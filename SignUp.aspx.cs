﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SignUp : System.Web.UI.Page
{
    VirtualScienceLabTableAdapters.SignUpsTableAdapter TASignUPs = new VirtualScienceLabTableAdapters.SignUpsTableAdapter();
    AccessDataTableAdapters.EmailTrackingTableAdapter TAEmailTracking = new AccessDataTableAdapters.EmailTrackingTableAdapter();
    AccessData.EmailTrackingRow rwEmail;

    string SendingEmail = "contact@solvexx.com";
    string SendingEmailUserName = "contact@solvexx";
    string SendingEmailPassword = "8a5a9534";
    string EmailServer = "mail.solvexx.com";

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Guid theVerifCode = Guid.NewGuid();

            TASignUPs.Insert(FirstName.Text, Name.Text, Position.Text, Department.Text, Organisation.Text, Email.Text, Tel.Text, theVerifCode.ToString());

            signupButtons.Visible = false;
            FirstName.ReadOnly = true;
            Name.ReadOnly = true;
            Position.ReadOnly = true;
            Department.ReadOnly = true;
            Organisation.ReadOnly = true;
            Email.ReadOnly = true;
            Tel.ReadOnly = true;

            string EMailSubject = "Virtual Science Lab Sign Up";
            string EMailBody = "";
            EMailBody = "Welcome to the Virtual Science Lab from Learnexx 3D.<br /><br />";
            EMailBody = EMailBody + "Please click on the following link to confirm your email address:<br /><br />";
            EMailBody = EMailBody + "<a href='http://virtualsciencelab.com?verifcode=" + theVerifCode.ToString() + "'>http://virtualsciencelab.com?verifcode=" + theVerifCode.ToString() + "</a>";

            rwEmail = EmailUtilities.SendEMail(Email.Text, SendingEmail, EMailSubject, EMailBody, SendingEmailUserName, SendingEmailPassword, "", "0", CommonUtilities.origType.Website, "0", EMailSubject, EmailServer);

            if (rwEmail != null) ResponseMessage.Text = "<br /><br />We have sent you an email, please click the link it contains to confirm your email address.";
            else ResponseMessage.Text = "<br /><br />There was an error sending you an email, please try to register again or contact us for support.";

            ResponseMessage.Visible = true;

            EMailBody = FirstName.Text + " " + Name.Text +"<br />" + Position.Text + "<br />" + Department.Text + "<br />" + Organisation.Text + "<br />" + Email.Text + "<br />" + Tel.Text;
            EmailUtilities.SendEMail("contact@solvexx.com", SendingEmail, EMailSubject, EMailBody, SendingEmailUserName, SendingEmailPassword, "", "0", CommonUtilities.origType.Website, "0", EMailSubject, EmailServer);
        }
    }
}