﻿Imports System.Net.Mail
Imports CommonUtilities

Public Module EmailUtilities
    Dim EmailServer As String = "mail.club-internet.fr"
    Dim EmailDeliveryReceipts As String = "No"
    Dim EmailReadReceipts As String = "No"
    Dim EmailLogging As String = "Yes"

    Public Function SendEMail(ByVal mailto As String, ByVal sender As String, _
                        ByVal subject As String, ByVal body As String, _
                        ByVal username As String, ByVal password As String, _
                        Optional ByVal attachmentFileName As String = "", _
                        Optional ByVal strRecipID As String = "", _
                        Optional ByVal originalType As origType = CommonUtilities.origType.Campaign, _
                        Optional ByVal origID As String = "", _
                        Optional ByVal optstrCustom As String = "", _
                        Optional ByVal optionalemailserver As String = "", _
                        Optional ByVal displayName As String = "") As AccessData.EMailTrackingRow

        'sends an email in html format using the .net.mail class
        Dim msg As New MailMessage
        Dim client As New SmtpClient()
        If optionalemailserver <> "" Then
            client.Host = optionalemailserver
        Else
            client.Host = EmailServer
        End If

        'to solve the bug 'User not local'
        If client.Host <> "mail.solvexx.com" Then
            client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
        End If

        'Set the statuses of the delivery and read receipt options from web config
        If EmailDeliveryReceipts = "Yes" Then
            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess
        Else
            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        End If
        If EmailReadReceipts = "Yes" Then
            msg.Headers.Add("Disposition-Notification-To", sender)
        End If

        msg.Headers.Add("Return-Path", sender)
        Try
            msg.To.Add(New MailAddress(mailto))
        Catch ex As Exception
            Exit Function
        End Try

        If displayName <> "" Then
            msg.From = New MailAddress(sender, displayName)
            msg.ReplyTo = New MailAddress(sender, displayName)
        Else
            msg.From = New MailAddress(sender)
            msg.ReplyTo = New MailAddress(sender)
        End If
        msg.Subject = subject
        msg.Body = body
        msg.IsBodyHtml = True
        If attachmentFileName <> "" Then
            msg.Attachments.Add(New Attachment(attachmentFileName))
        End If
        'This object stores the authentication values
        Dim basicAuthenticationInfo As _
           New System.Net.NetworkCredential(username, password)
        client.UseDefaultCredentials = False
        client.Credentials = basicAuthenticationInfo
        Dim EmailTrackID As String = ""
        Dim TAEmailTrack As New AccessDataTableAdapters.EmailTrackingTableAdapter

        'If the tracking email flag is on
        If EmailLogging = "Yes" Then
            'Creates an email tracking record and returns the ID
            EmailTrackID = TAEmailTrack.InsertEmail(strRecipID, mailto, Now(), originalType, origID, optstrCustom, "")
        End If

        Dim DTEmailTrack As AccessData.EmailTrackingDataTable = TAEmailTrack.TrackEmail()
        Dim rwEmailTrack As AccessData.EmailTrackingRow = DTEmailTrack.FindByEMailTrackingID(EmailTrackID)
        Try
            client.Send(msg)
        Catch e1 As Exception
            If EmailLogging = "Yes" Then
                rwEmailTrack.eResult = e1.ToString
                TAEmailTrack.Update(rwEmailTrack)
            End If
            SendEMail = rwEmailTrack
            Exit Function
        End Try
        If EmailLogging = "Yes" Then
            rwEmailTrack.eResult = "Email Sent"
            TAEmailTrack.Update(rwEmailTrack)
        End If
        SendEMail = rwEmailTrack
    End Function

    Function ReturnBetween2Strings(ByVal strPage As String, ByVal StartString As String, ByVal EndString As String) As String
        'Find a start string and an end string and return what is between the two strings - searches forwards through the strPage
        'Find the start position of the first string in the page
        Dim startPos As Integer = InStr(strPage, StartString, CompareMethod.Text)
        Dim StartStringFromPage As String

        'returns an empty string if not found
        ReturnBetween2Strings = ""

        'if string is not in the files then exit
        If startPos = 0 Then
            Exit Function
        End If

        'check to see if the string case matches and if not exit
        StartStringFromPage = Mid(strPage, startPos, Len(StartString))
        If [String].CompareOrdinal(StartString, StartStringFromPage) <> 0 Then
            Exit Function
        End If

        'Find the position of the first occurance of the second string after the end of the first string
        Dim endpos As Integer = InStr(startPos + Len(StartString), strPage, EndString)
        'The string to be returned starts at the end of the first string
        Dim returnStart As Integer = startPos + Len(StartString)
        'return the string between the end of the first string and the beginning of the second string
        If startPos < endpos Then
            ReturnBetween2Strings = Mid(strPage, returnStart, endpos - returnStart)
        End If
    End Function

    Function RemoveBetween2Strings(ByVal strPage As String, ByVal StartString As String, ByVal EndString As String) As String
        'Find a start string and an end string and remove the strings and everything between them and then return the remaining string
        'Find the start position of the first string in the page
        Dim startPos As Integer = InStr(strPage, StartString)
        'return an empty string if error
        RemoveBetween2Strings = ""

        'if string is not in the files then exit
        If startPos = 0 Then
            Exit Function
        End If
        'Find the position of the first occurance of the second string after the end of the first string
        Dim endpos As Integer = InStr(startPos + Len(StartString) - 1, strPage, EndString)
        'return the string before the start of the first string and after the end of the second string
        If startPos < endpos Then
            RemoveBetween2Strings = Left(strPage, startPos - 1) & Right(strPage, Len(strPage) + 1 - (endpos + Len(EndString)))
        End If

    End Function

    Function ReplaceToken(ByVal strOriginal As String, ByVal strToken As String, ByVal replaceBy As String) As String
        ReplaceToken = strOriginal
        Dim startPos As Integer = InStr(strOriginal, strToken)
        'return an empty string if error or Token not found
        ReplaceToken = ""

        'if string is not in the files then exit
        If startPos = 0 Then
            Exit Function
        End If
        'replace the Token
        ReplaceToken = Left(strOriginal, startPos - 1) & replaceBy & Right(strOriginal, Len(strOriginal) + 1 - (startPos + Len(strToken)))

        Dim strFinal As String = ""
        'recursive call in case token used several times
        While ReplaceToken <> ""
            strFinal = ReplaceToken
            ReplaceToken = ReplaceToken(strFinal, strToken, replaceBy)
        End While

        ReplaceToken = strFinal
    End Function

    Public Function RemoveFromFirstOccurrenceOfStringToBeginningOfString(ByVal strOriginal As String, ByVal searchstring As String) As String
        'removes everything (including the string) after the first occurrence of a string to the beginning of the string
        'Returns the original string if not found
        Dim result As String = strOriginal
        Dim Position As Integer
        Position = InStr(strOriginal, searchstring)
        If Position <> 0 Then
            result = Right(strOriginal, strOriginal.Length - Position - searchstring.Length + 1)
        End If
        Return result
    End Function

    Public Function RemoveFromFirstOccurrenceOfStringToEndOfString(ByVal strOriginal As String, ByVal searchstring As String) As String
        'removes everything (including the string) after the first occurrence of a string to the end of the string
        'Returns "" if not found
        Dim result As String = ""
        Dim Position As Integer
        Position = InStr(strOriginal, searchstring)
        If Position <> 0 Then
            result = Left(strOriginal, Position - 1)
        End If
        Return result
    End Function
End Module

