Cufon.replace('h1 .big, h2', { fontFamily: 'Arial Rounded MT Bold'});
Cufon.replace('h1 .small', { fontFamily: 'PetitaLight'});
Cufon.replace('.menu a, .tabs a', { fontFamily: 'Myriad Pro', hover: true });
Cufon.replace('.banner1 .tite', { fontFamily: 'Myriad Pro', textShadow: '#13a6e2 1px 1px'});
Cufon.replace('.banner2 .tite', { fontFamily: 'Myriad Pro', textShadow: '#cf5c0e 1px 1px'});
Cufon.replace('.banner3 .tite', { fontFamily: 'Myriad Pro', textShadow: '#469a0d 1px 1px'});