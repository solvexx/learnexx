addEvent(window, 'load', initCorners);

  function initCorners() {
    var settings1 = {
      tl: { radius: 8 },
      tr: { radius: 8 },
      bl: { radius: 8 },
      br: { radius: 8 },
      antiAlias: true
    } 
	
    curvyCorners(settings1, ".nifty");
  }